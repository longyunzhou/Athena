﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Athena.basedal;
using Athena.dal;
using Athena.bll;
using Athena.common.Cache.Factory;
using System.Web;
using System.Web.Mvc;

namespace SSO.Web
{
    public class LoginIn:Controller
    {
        protected AccountsPrincipal acctx = null;//当前用户上下文对象
        
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                DB.GetInstance();
                Athena.basedal.IocModule i = new IocModule();
                context.Response.ContentType = "text/plain";
                if (context.Request["action"] == "login")
                {
                    string userName = context.Request["username"];
                    string password = context.Request["password"];
                    acctx = AccountsPrincipal.ValidateLogin(userName, password);
                    if (acctx != null)
                    {
                        string userLoginId = Guid.NewGuid().ToString();
                        CacheFactory.Cache().WriteCache<AccountsPrincipal>(acctx, userLoginId, DateTime.Now.AddMinutes(20));
                        HttpCookie userLoginInCookie = new HttpCookie("userLoginId");
                        //
                        userLoginInCookie.Value = userLoginId;
                        context.User = acctx; System.Web.HttpContext.Current.Response.Cookies.Add(userLoginInCookie);
                        context.Response.Write("true");
                    }
                }
            }
            catch (Exception e)
            {
                context.Response.Write(e.ToString());
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
